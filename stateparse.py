import asyncio
import socket
import pprint
import sys
import json
import requests

statefile = sys.argv[1]
environment = sys.argv[2]

if environment == "prod":
    key = "f67eecc6-4659-44fd-a4fd-8816c993ad0e"
else:
    key = "e8d8280a-a00c-412c-be5c-9f2186fd8280"


async def connected(reader, writer):
    data = bytearray()
    while True:
        chunk = await reader.read(1024)
        data += chunk
        if not chunk:
            break
    message = json.loads(data.decode())
    if message['task'] == 'update_server':
        update_server(message['body'])
    elif message['task'] == 'update_state':
        update_state(message['body'])

def update_state(state):
    with open(statefile, 'w') as f:
        f.write(state)

    print("State updated")

def get_srs_client_list_file():
    srs_file = "C:\Program Files\DCS-SimpleRadio-Standalone\clients-list.json"
    with open(srs_file,'r') as f:
        return json.load(f)

def srs_client_map(srs_client):
    mapped_client = {}
    mapped_client['name'] = srs_client['Name']
    radInfo = srs_client['RadioInfo']
    if radInfo:
        mapped_client['unit']= radInfo['unit']
        freq = str(radInfo['radios'][radInfo['selected']]['freq'])
        mapped_client['freq'] = freq[0:3] + "." + freq[4:7]
    return mapped_client

def get_srs_clients():
    clients = get_srs_client_list_file()
    return list(map(lambda x: srs_client_map(x), clients))

def update_server(mission_data):
    server_update = {
        "key": key,
        "status": {
            "map": mission_data['theater'],
            "players": mission_data['players'],
            "maxPlayers": mission_data['maxPlayers'],
            "serverName": mission_data['serverName'],
            "missionName": mission_data['missionName'],
            "data": {
                "updateTime": mission_data['updateTime'],
                "uptime": mission_data['uptime'],
                "metar": mission_data['metar'],
                "SRSClients": get_srs_clients()
            }
        },
    }

    #conn = socket.create_connection(("status.hoggitworld.com", 29587))
    #conn.send((json.dumps(server_update) + "\n").encode())
    #conn.close()

    r = requests.post("https://status.hoggitworld.com/{0}".format(key), json=server_update)

    with open('state.json', 'w') as f:
        f.write(json.dumps(mission_data))
	
    if environment == 'dev':
        pprint.pprint(json.dumps(server_update))
    print('Server Status updated')


loop = asyncio.get_event_loop()
listen = asyncio.start_server(connected, '127.0.0.1', 10400)
server = loop.run_until_complete(listen)
print("Created listener.")

try:
    print('Starting listener')
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
loop.close()
