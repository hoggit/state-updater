-- User editable configs
local object_batch = 500  --How many objects to iterate before yielding

-- Load JSON lib
fs = require('lfs')
package.path = fs.currentdir() .. "Scripts\\?.lua;" .. package.path
socket = require("socket")
local jsonlib = fs.writedir() .. "Scripts\\GAW\\json.lua"
json = loadfile(jsonlib)()

-- Testing
--key = "e8d8280a-a00c-412c-be5c-9f2186fd8280"
-- Production
key = "f67eecc6-4659-44fd-a4fd-8816c993ad0e"

function split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

-- Setup logging
logFile = io.open(fs.writedir()..[[Logs\HoggitState.log]], "w")

function log(str)
    if str == nil then str = 'nil' end
    if logFile then
       logFile:write("HOGGIT STATE LOG - " .. str .."\r\n")
       logFile:flush()
    end

    net.log("HSR - " .. str)
end

log("LOG INIT")

function loadMetar()
    local file = io.open(fs.writedir()..[[Scripts\ServerState\metar]], "r")
    if file then
        local metar = file:read()
        file:close()
        return metar
    end
    return ""
end

-- Setup some state
local last_update = 0
local running = false
local theater = ""
local missionName = ""
local serverName = ""
local maxPlayers = 0
local wx = ""
local co = nil
local update_time = 1
local start_time = 0
local metar = ""

local HoggitStateCallbacks = {}

-- Setup the export function
ExportStatus = function()
    while true do
        log("Starting Export")
        local exports = {}
        local airports = {}
        local o = Export.LoGetWorldObjects()
        local a = Export.LoGetWorldObjects('airdromes')
        local idx = 0

        for k,v in pairs(o) do
            v.id = k
            table.insert(exports, v)
            idx = idx + 1
            if idx >= object_batch then
                idx = 0
                log('batched ' .. #exports)
                coroutine.yield(1)
            end
        end

        for k,v in pairs(a) do
            v.id = k
            table.insert(airports, v)
            idx = idx + 1
            if idx >= object_batch then
                idx = 0
                log('batched ' .. #airports)
                coroutine.yield(1)
            end
        end

        --local statefile = io.open(fs.writedir() .. [[Scripts\HoggitGCI\serverstate_test.json]], "w")
        log("Statefile open")
        --statefile:write(net.lua2json({airports=airports, players=#net.get_player_list(), uptime = DCS.getModelTime(), updateTime = os.date('!%Y-%m-%dT%H:%M:%SZ'), objects = exports, theater = theater, missionName = missionName, serverName = serverName, maxPlayers = maxPlayers}))
        --statefile:close()
        local data = {
            task = 'update_server',
            body = {
                airports=airports, 
                players=#net.get_player_list(), 
                uptime = DCS.getModelTime(), 
                realtime = DCS.getRealTime(), 
                updateTime = os.date('!%Y-%m-%dT%H:%M:%SZ'),
                start_time = start_time,

                objects = exports, 
                theater = theater, 
                missionName = missionName, 
                serverName = serverName, 
                maxPlayers = maxPlayers,
                wx = wx,
                metar = metar
            }
        }

        local conn = socket.tcp()
        conn:connect('127.0.0.1', 10400)

        conn:send(net.lua2json(data))
        conn:close()
        log("Statefile closed")
        coroutine.yield(10)
    end
end

log("Finished setting up exportstatus function")

HoggitStateCallbacks.onMissionLoadEnd = function()
    log("Mission has loaded")
    -- Get server settings
    dofile(fs.writedir()..[[Config\serverSettings.lua]])
    log("Got server settings for server " .. cfg['name'])
    theater = DCS.getCurrentMission()['mission']['theatre']
    wx = DCS.getCurrentMission()['mission']['weather']
    start_time = DCS.getCurrentMission()['mission']['start_time']
    missionName = DCS.getMissionName()
    serverName = cfg['name']
    maxPlayers = cfg['maxPlayers']
    metar = loadMetar()
    log('Finished parsing cfg data')
end

HoggitStateCallbacks.onSimulationFrame = function()
    if running and DCS.getRealTime() > last_update + update_time then
        last_update = DCS.getRealTime()
        success, next = coroutine.resume(co)
        update_time = next

        if not success then log(next) end
    end
end

HoggitStateCallbacks.onSimulationStart = function()
    log("Simulation start")
    co = coroutine.create(ExportStatus)
    running = true
end

HoggitStateCallbacks.onSimulationStop = function()
    running = false
    log("Simulation stop")
    logFile:close()
end

DCS.setUserCallbacks(HoggitStateCallbacks)
net.log("Loaded - Hoggit Status Report (HSR) v1.0.0")